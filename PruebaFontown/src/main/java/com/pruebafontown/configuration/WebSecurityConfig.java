package com.pruebafontown.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService user;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(user).passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public static BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/resources/**").permitAll()
			.antMatchers("/admin/**").hasAuthority("Admin")
			.antMatchers("/usuario/**").hasAnyAuthority("Admin", "Usuario")
			.antMatchers("/producto/**").hasAnyAuthority("Admin", "Profesor", "Usuario")
			.anyRequest().authenticated().and()
			.formLogin().usernameParameter("email").passwordParameter("password")
			.loginProcessingUrl("j_spring_security_check")
			.loginPage("/login").failureUrl("/login?error=true")
			.defaultSuccessUrl("/home")
			.permitAll()
			.and()
			.logout()
			.permitAll();
	}
	

}
