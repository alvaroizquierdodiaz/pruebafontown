package com.pruebafontown.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = "com.pruebafontown.*")
@EnableJpaRepositories(basePackages = "com.pruebafontown.repository")
@EnableTransactionManagement
@SpringBootApplication
public class FontownConfiguration {
	
	public static void main(String[] args) throws Exception {
        SpringApplication.run(FontownConfiguration.class, args);
    }

}
