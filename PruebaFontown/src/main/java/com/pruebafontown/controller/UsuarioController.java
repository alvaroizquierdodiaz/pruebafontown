package com.pruebafontown.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UsuarioController {
	
	@GetMapping(value = {"/", "/login"})
	public static ModelAndView login() {
		ModelAndView mw = new ModelAndView();
		mw.setViewName("login");
		return mw;
	}
	
	@GetMapping(value="/home")
	public static ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
		modelAndView.setViewName("home");
		return modelAndView;
	}
	
	

}
