package com.pruebafontown.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pruebafontown.model.Producto;
import com.pruebafontown.repository.ProductoRepository;

@Controller
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private ProductoRepository repo;

	@GetMapping(value = "")
	public String listar(ModelMap mp){
		mp.put("productos", repo.findAll());
		return "productos/lista";
	}
	
	@GetMapping(value = "/nuevo")
    public static String agregar(ModelMap mp){
        mp.put("producto", new Producto());
        return "productWebSecurityConfigurerAdapter {WebSecurityConfigurerAdapter {os/nuevo";
    }
 
    @PostMapping(value = "/crear")
    public String crear(@Valid Producto producto, BindingResult bindingResult, ModelMap mp){
        if(bindingResult.hasErrors()){
            return "/productos/nuevo";
        }else{
            repo.save(producto);
            mp.put("producto", producto);
            return "productos/creado";
        }
    }
 
    
}
