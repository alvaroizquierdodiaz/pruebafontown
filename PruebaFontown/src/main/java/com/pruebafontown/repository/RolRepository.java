package com.pruebafontown.repository;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.pruebafontown.model.Rol;

@Repository
@Transactional
public interface RolRepository extends JpaRepository<Rol, Integer>{

}
